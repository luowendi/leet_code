import java.util.HashMap;
import java.util.Map;

/**
 * 给你一个链表的头节点 head，请你编写代码，反复删去链表中由 总和 值为 0 的连续节点组成的序列，直到不存在这样的序列为止
 */
public class leetcode1171 {

    /**
     * 解法一
     * @param head
     * @return
     */
    public ListNode removeZeroSumSublists(ListNode head) {
        if(head == null)  return null;
        int sum = 0;
        for(ListNode cur = head; cur!=null;cur=cur.next){
            if((sum += cur.val) == 0){
                return removeZeroSumSublists(cur.next); // 此处计算一开始就为零
            }
        }
        head.next = removeZeroSumSublists(head.next); // 此处起到临界作用
        return head;
    }

    public ListNode removeZeroSumSublists1(ListNode head) {
        if(head == null) return  null;
        Map<Integer,ListNode> seen = new HashMap<>();
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        int prefix = 0;
        for(ListNode node = dummy; node!= null; node = node.next){
            prefix += node.val;
            seen.put(prefix,node);
        }
        prefix = 0;
        for(ListNode node = dummy;node!=null;node=node.next){
            prefix+=node.val;
            node.next = seen.get(prefix).next;
        }
        return dummy.next;
    }

    public class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }

}
